<?php

class LoginController extends BaseController {
	
	protected $layout = 'layouts.login';

	public function showWelcome()
	{
		$view = View::make('login.index');
		$this->layout->title = 'CMS - Login';		
		$this->layout->content = $view;
	}

}
