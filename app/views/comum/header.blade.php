<div class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">CMS - Auto Creator</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>            
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Configurações <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Módulos</a></li>
                <li><a href="#">Perfis</a></li>
                <li><a href="#">Usuários</a></li>
              </ul>
              <li><a href="#">Sair</a></li>    
            </li>
          </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>