<form class="form-signin" role="form" method="POST" action='<?=URL::to('login')?>'>
	<h2 class="form-signin-heading">Efetue o Login</h2>
	<input type="email" name="email" class="form-control" placeholder="Email" required="required">
	<input type="password" name="senha" class="form-control" placeholder="Senha" required="required">
	<label class="checkbox">
		<input type="checkbox" value="remember-me">Lembrar-me
	</label>
	<button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
	<a href="#" class="esqueceu_senha pull-left" data-toggle="modal" data-target="#myModal" style="margin-top: 5px;">Esqueceu sua senha?</a>
</form>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Esqueceu sua senha?</h4>
      </div>
      <form method="post">
	      <div class="modal-body">
	      	<label for="email_esqueci" class="email">Digite seu e-mail de cadastro:</label>
	        <input type="email" name="email_esqueci" id="email_esqueci" class="form-control email_esqueci" placeholder="Email" required="required">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Enviar</button>
	      </div>
      </form>
    </div>
  </div>
</div>